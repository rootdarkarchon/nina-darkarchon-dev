﻿<!--
    Copyright © 2016 - 2020 Stefan Berg <isbeorn86+NINA@googlemail.com> and the N.I.N.A. contributors

    This file is part of N.I.N.A. - Nighttime Imaging 'N' Astronomy.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.-->
<UserControl
    x:Class="NINA.View.SimpleSequencer.SimpleSequenceView"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:filter="clr-namespace:NINA.Model.MyFilterWheel"
    xmlns:local="clr-namespace:NINA.View.SimpleSequencer"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:model="clr-namespace:NINA.Model"
    xmlns:ninactrl="clr-namespace:NINACustomControlLibrary;assembly=NINACustomControlLibrary"
    xmlns:ns="clr-namespace:NINA.Locale"
    xmlns:rules="clr-namespace:NINA.Utility.ValidationRules"
    xmlns:s="clr-namespace:System;assembly=mscorlib"
    xmlns:util="clr-namespace:NINA.Utility"
    xmlns:view="clr-namespace:NINA.View"
    x:Name="UC"
    d:DesignHeight="450"
    d:DesignWidth="800"
    mc:Ignorable="d">

    <UserControl.Resources>
        <ResourceDictionary Source="../Sequence2/ProgressStyle.xaml" />
    </UserControl.Resources>
    <Grid>
        <Grid.Resources>
            <util:BindingProxy x:Key="nighttimeProxy" Data="{Binding NighttimeData}" />
        </Grid.Resources>
        <Grid.RowDefinitions>
            <RowDefinition />
            <RowDefinition Height="45" />
        </Grid.RowDefinitions>

        <ScrollViewer VerticalScrollBarVisibility="Auto">
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="Auto" />
                    <RowDefinition />
                    <RowDefinition Height="Auto" />
                </Grid.RowDefinitions>

                <Grid Margin="5,0,0,0">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition />
                        <ColumnDefinition Width="Auto" />
                    </Grid.ColumnDefinitions>

                    <ListView
                        MaxHeight="65"
                        Margin="-5,0,0,0"
                        BorderBrush="Transparent"
                        ItemsSource="{Binding Sequencer.MainContainer.Items[1].Items}"
                        ScrollViewer.HorizontalScrollBarVisibility="Disabled"
                        ScrollViewer.VerticalScrollBarVisibility="Auto"
                        SelectedItem="{Binding SelectedTarget}">
                        <ListView.Resources>
                            <Style x:Key="myHeaderStyle" TargetType="{x:Type GridViewColumnHeader}">
                                <Setter Property="Visibility" Value="Collapsed" />
                            </Style>
                        </ListView.Resources>
                        <ListView.ItemsPanel>
                            <ItemsPanelTemplate>
                                <WrapPanel Orientation="Horizontal" />
                            </ItemsPanelTemplate>
                        </ListView.ItemsPanel>
                        <ListView.ItemContainerStyle>
                            <Style TargetType="ListViewItem">
                                <Setter Property="Template">
                                    <Setter.Value>
                                        <ControlTemplate TargetType="ListViewItem">
                                            <Grid Width="200" Height="30">
                                                <Polygon
                                                    x:Name="PART_Polygon"
                                                    Margin="-38,0,0,0"
                                                    Fill="{StaticResource ButtonBackgroundBrush}"
                                                    Points="0,0 10,0 12,5 10,10 0,10 2,5 0,0"
                                                    Stretch="Fill" />
                                                <Grid
                                                    MaxWidth="130"
                                                    HorizontalAlignment="Stretch"
                                                    VerticalAlignment="Center">
                                                    <Grid.ColumnDefinitions>
                                                        <ColumnDefinition Width="Auto" />
                                                        <ColumnDefinition />
                                                        <ColumnDefinition Width="Auto" />
                                                        <ColumnDefinition Width="Auto" />
                                                    </Grid.ColumnDefinitions>
                                                    <ContentPresenter
                                                        Grid.Column="0"
                                                        HorizontalAlignment="Left"
                                                        VerticalAlignment="Center"
                                                        Content="{Binding}"
                                                        Style="{StaticResource ProgressPresenter}" />
                                                    <TextBlock
                                                        Grid.Column="1"
                                                        Margin="2,0,0,0"
                                                        HorizontalAlignment="Left"
                                                        VerticalAlignment="Center"
                                                        Foreground="{StaticResource ButtonForegroundBrush}"
                                                        Text="{Binding Target.TargetName}" />
                                                    <Button
                                                        x:Name="PART_Reset"
                                                        Grid.Column="2"
                                                        Width="25"
                                                        Height="25"
                                                        Margin="2,0,0,0"
                                                        HorizontalAlignment="Right"
                                                        Command="{Binding ResetProgressCommand}"
                                                        CommandParameter="{Binding}">
                                                        <Button.ToolTip>
                                                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                                                <TextBlock Text="{ns:Loc LblTooltipResetTarget}" />
                                                            </ToolTip>
                                                        </Button.ToolTip>
                                                        <Grid>
                                                            <Path
                                                                Margin="5"
                                                                Data="{StaticResource LoopSVG}"
                                                                Fill="{StaticResource ButtonForegroundBrush}"
                                                                Stretch="Uniform" />
                                                        </Grid>
                                                    </Button>
                                                    <Button
                                                        x:Name="PART_Close"
                                                        Grid.Column="3"
                                                        Width="25"
                                                        Height="25"
                                                        Margin="2,0,0,0"
                                                        HorizontalAlignment="Right"
                                                        Command="{Binding DetachCommand}"
                                                        CommandParameter="{Binding}">

                                                        <Path
                                                            Margin="5"
                                                            Data="{StaticResource TrashCanSVG}"
                                                            Fill="{StaticResource ButtonForegroundBrush}"
                                                            Stretch="Uniform" />
                                                    </Button>
                                                </Grid>
                                            </Grid>
                                            <ControlTemplate.Triggers>
                                                <Trigger Property="IsSelected" Value="True">
                                                    <Setter TargetName="PART_Polygon" Property="Fill" Value="{StaticResource ButtonBackgroundSelectedBrush}" />
                                                </Trigger>
                                                <Trigger Property="IsMouseOver" Value="False">
                                                    <Setter TargetName="PART_Close" Property="Visibility" Value="Collapsed" />
                                                </Trigger>
                                                <Trigger Property="IsMouseOver" Value="False">
                                                    <Setter TargetName="PART_Reset" Property="Visibility" Value="Collapsed" />
                                                </Trigger>
                                            </ControlTemplate.Triggers>
                                        </ControlTemplate>
                                    </Setter.Value>
                                </Setter>
                            </Style>
                        </ListView.ItemContainerStyle>
                        <ListView.View>
                            <GridView ColumnHeaderContainerStyle="{StaticResource myHeaderStyle}" />
                        </ListView.View>
                    </ListView>
                    <StackPanel Grid.Column="1" Orientation="Horizontal">
                        <Button
                            Grid.Column="1"
                            Width="35"
                            Command="{Binding SelectedTarget.MoveUpCommand}">
                            <Button.ToolTip>
                                <ToolTip ToolTipService.ShowOnDisabled="True">
                                    <TextBlock Text="{ns:Loc LblTooltipPromoteTarget}" />
                                </ToolTip>
                            </Button.ToolTip>
                            <Path
                                Margin="5"
                                Data="{StaticResource ArrowLeftSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Button>
                        <Button
                            Grid.Column="2"
                            Width="35"
                            Command="{Binding SelectedTarget.MoveDownCommand}">
                            <Button.ToolTip>
                                <ToolTip ToolTipService.ShowOnDisabled="True">
                                    <TextBlock Text="{ns:Loc LblTooltipDemoteTarget}" />
                                </ToolTip>
                            </Button.ToolTip>
                            <Path
                                Margin="5"
                                Data="{StaticResource ArrowRightSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Button>
                        <Button
                            Grid.Column="3"
                            Width="35"
                            Command="{Binding SaveTargetSetCommand}">
                            <Button.ToolTip>
                                <ToolTip ToolTipService.ShowOnDisabled="True">
                                    <TextBlock Text="{ns:Loc LblTooltipTargetSaveSet}" />
                                </ToolTip>
                            </Button.ToolTip>
                            <Path
                                Margin="5"
                                Data="{StaticResource SaveSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Button>
                        <Button
                            Grid.Column="4"
                            Width="35"
                            Command="{Binding AddTargetCommand}">
                            <Button.ToolTip>
                                <ToolTip ToolTipService.ShowOnDisabled="True">
                                    <TextBlock Text="{ns:Loc LblTooltipAddTarget}" />
                                </ToolTip>
                            </Button.ToolTip>
                            <Path
                                Margin="5"
                                Data="{StaticResource AddSVG}"
                                Fill="{StaticResource ButtonForegroundBrush}"
                                Stretch="Uniform" />
                        </Button>
                    </StackPanel>
                </Grid>

                <local:SimpleDSOContainerView Grid.Row="2" DataContext="{Binding SelectedTarget}" />

                <Border
                    Grid.Row="3"
                    BorderBrush="{StaticResource BorderBrush}"
                    BorderThickness="0,0,0,1">
                    <Grid Margin="5,0,0,0">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition />
                            <ColumnDefinition Width="Auto" />
                            <ColumnDefinition />
                        </Grid.ColumnDefinitions>
                        <GroupBox>
                            <GroupBox.Header>
                                <TextBlock
                                    VerticalAlignment="Center"
                                    FontSize="16"
                                    Text="{ns:Loc Lbl_OldSequencer_StartOfSequenceHeader}" />
                            </GroupBox.Header>
                            <UniformGrid Columns="2" DataContext="{Binding StartOptions}">
                                <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                                    <TextBlock
                                        MinWidth="200"
                                        MinHeight="20"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblCoolCamAtSequenceStart}" />
                                    <CheckBox HorizontalAlignment="Left" IsChecked="{Binding CoolCameraAtSequenceStart, Mode=TwoWay}" />
                                </StackPanel>
                                <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                                    <TextBlock
                                        MinWidth="200"
                                        MinHeight="20"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblUnparkMountAtSequenceStart}" />
                                    <CheckBox HorizontalAlignment="Left" IsChecked="{Binding UnparkMountAtSequenceStart, Mode=TwoWay}" />
                                </StackPanel>

                                <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                                    <TextBlock
                                        MinWidth="200"
                                        MinHeight="20"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblAutoMeridianFlip}" />
                                    <CheckBox HorizontalAlignment="Left" IsChecked="{Binding DataContext.DoMeridianFlip, Mode=TwoWay, ElementName=UC}" />
                                </StackPanel>
                            </UniformGrid>
                        </GroupBox>

                        <Border
                            Grid.Column="1"
                            BorderBrush="{StaticResource BorderBrush}"
                            BorderThickness="0.5" />

                        <GroupBox Grid.Column="2">
                            <GroupBox.Header>
                                <TextBlock
                                    VerticalAlignment="Center"
                                    FontSize="16"
                                    Text="{ns:Loc Lbl_OldSequencer_EndOfSequenceHeader}" />
                            </GroupBox.Header>
                            <Grid DataContext="{Binding EndOptions}">
                                <Grid.ColumnDefinitions>
                                    <ColumnDefinition />
                                    <ColumnDefinition />
                                </Grid.ColumnDefinitions>
                                <Grid.RowDefinitions>
                                    <RowDefinition />
                                    <RowDefinition />
                                </Grid.RowDefinitions>
                                <StackPanel Margin="0,5,0,0" Orientation="Horizontal">
                                    <TextBlock
                                        MinWidth="200"
                                        MinHeight="20"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblWarmCamAtSequenceEnd}" />
                                    <CheckBox HorizontalAlignment="Left" IsChecked="{Binding WarmCamAtSequenceEnd, Mode=TwoWay}" />
                                </StackPanel>
                                <StackPanel
                                    Grid.Column="1"
                                    Margin="0,5,0,0"
                                    Orientation="Horizontal">
                                    <TextBlock
                                        MinWidth="200"
                                        MinHeight="20"
                                        VerticalAlignment="Center"
                                        Text="{ns:Loc LblParkMountAtSequenceEnd}" />
                                    <CheckBox HorizontalAlignment="Left" IsChecked="{Binding ParkMountAtSequenceEnd, Mode=TwoWay}" />
                                </StackPanel>
                            </Grid>
                        </GroupBox>
                    </Grid>
                </Border>
            </Grid>
        </ScrollViewer>

        <DockPanel Grid.Row="2" Margin="5">
            <StackPanel DockPanel.Dock="Left" Orientation="Horizontal">
                <StackPanel Grid.Row="2" Orientation="Horizontal">
                    <Button
                        Width="40"
                        Command="{Binding SwitchToOverviewCommand}"
                        Visibility="{Binding Source={StaticResource ProfileService}, Path=ActiveProfile.SequenceSettings.DisableSimpleSequencer, Converter={StaticResource InverseBooleanToVisibilityCollapsedConverter}}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc Lbl_Sequencer_SwitchToOverview}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource BackSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>
                    <Button
                        Width="40"
                        Margin="1,0,0,0"
                        Command="{Binding SelectedTarget.AddSimpleExposureCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblTooltipAddSequence}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource AddSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>
                    <Button
                        Width="40"
                        Margin="1,0,0,0"
                        Command="{Binding SelectedTarget.RemoveSimpleExposureCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblTooltipRemoveSequence}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource TrashCanSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>

                    <Button
                        Width="40"
                        Margin="1,0,0,0"
                        Command="{Binding SelectedTarget.ResetSimpleExposureCommand}"
                        CommandParameter="{Binding}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblTooltipResetSequence}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="4"
                            Data="{StaticResource LoopSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>

                    <Button
                        Width="40"
                        Margin="1,0,0,0"
                        Command="{Binding SelectedTarget.PromoteSimpleExposureCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblTooltipPromoteSimpleExposure}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource ArrowUpSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>

                    <Button
                        Width="40"
                        Margin="1,0,0,0"
                        Command="{Binding SelectedTarget.DemoteSimpleExposureCommand}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblTooltipDemoteSimpleExposure}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource ArrowDownSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>
                </StackPanel>
                <Button
                    Width="40"
                    Margin="1,0,0,0"
                    Command="{Binding SaveSequenceCommand}">
                    <Button.ToolTip>
                        <ToolTip ToolTipService.ShowOnDisabled="True">
                            <TextBlock Text="{ns:Loc LblSaveSequenceToolTip}" />
                        </ToolTip>
                    </Button.ToolTip>
                    <Path
                        Margin="5"
                        Data="{StaticResource SaveSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                </Button>

                <Button
                    Width="40"
                    Margin="1,0,0,0"
                    Command="{Binding SaveAsSequenceCommand}">
                    <Button.ToolTip>
                        <ToolTip ToolTipService.ShowOnDisabled="True">
                            <TextBlock Text="{ns:Loc LblSaveAsSequenceToolTip}" />
                        </ToolTip>
                    </Button.ToolTip>
                    <Path
                        Margin="5"
                        Data="{StaticResource SaveAsSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                </Button>

                <Button
                    Width="40"
                    Margin="1,0,0,0"
                    Command="{Binding LoadSequenceCommand}">
                    <Button.ToolTip>
                        <ToolTip ToolTipService.ShowOnDisabled="True">
                            <TextBlock Text="{ns:Loc LblLoadSequenceToolTip}" />
                        </ToolTip>
                    </Button.ToolTip>
                    <Path
                        Margin="5"
                        Data="{StaticResource LoadSVG}"
                        Fill="{StaticResource ButtonForegroundBrush}"
                        Stretch="Uniform" />
                </Button>
            </StackPanel>
            <StackPanel HorizontalAlignment="Right" Orientation="Horizontal">
                <Grid MinWidth="100">
                    <Button Command="{Binding BuildSequenceCommand}" IsEnabled="{Binding IsRunning, Converter={StaticResource InverseBooleanConverter}}">
                        <Button.ToolTip>
                            <ToolTip ToolTipService.ShowOnDisabled="True">
                                <TextBlock Text="{ns:Loc LblBuildSequenceTooltip}" />
                            </ToolTip>
                        </Button.ToolTip>
                        <Path
                            Margin="5"
                            Data="{StaticResource SketchSVG}"
                            Fill="{StaticResource ButtonForegroundBrush}"
                            Stretch="Uniform" />
                    </Button>
                </Grid>
                <Grid MinWidth="300" Margin="5,0,0,0">
                    <ninactrl:AsyncProcessButton
                        ButtonForegroundBrush="{StaticResource ButtonForegroundBrush}"
                        ButtonImage="{StaticResource PlaySVG}"
                        CancelButtonImage="{StaticResource CancelSVG}"
                        CancelCommand="{Binding CancelSequenceCommand}"
                        CancelToolTip="{ns:Loc LblCancelSequence}"
                        Command="{Binding StartSequenceCommand}"
                        LoadingImageBrush="{StaticResource PrimaryBrush}"
                        ToolTip="{ns:Loc LblTooltipStartSequence}" />
                </Grid>
            </StackPanel>
        </DockPanel>
    </Grid>
</UserControl>